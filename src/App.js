import './App.css';
import { StylesProvider } from "@material-ui/core/styles";
import React, {Fragment} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Components/Style.css';
import FolderList from './Components/FolderList';
import CheckList from './Components/CheckList';
import ViewCheckList from './Components/ViewCheckList';
import AddChecklist from './Components/AddChecklist';
import EditChecklist from './Components/EditChecklist';
import {  BrowserRouter as Router,
  Routes,
  Route  } from "react-router-dom";
//import {  BrowserRouter as Router,Routes  } from "react-router-dom";
function App() {
  return (
    <Router>
      {/* <div className='box'></div> */}
        <div className="container">
  
        <Fragment>
        <StylesProvider injectFirst>
          <Routes>
            <Route exact path="/" element={<FolderList/>}></Route>
            <Route path="/checklist/:folderId" element={<CheckList/>}></Route>
            <Route path="/viewchecklist/:folderId/:checklistId" element={<ViewCheckList/>}></Route>
            <Route path="/addchecklist/:folderId" element={<AddChecklist/>}></Route>
            <Route path="/editchecklist/:folderId/:checklistId" element={<EditChecklist/>}></Route>
          </Routes>
      </StylesProvider>
      </Fragment>
      </div>
      {/* <div className='box1'></div> */}
  </Router>
      //  <div>
      //   <div className='box'></div>
      //   <div className="container">
      //     <FolderList />
      //   </div>
      //   <div className='box1'></div>
      //   <Switch>
      //   <Route exact path="/">
      //   <FolderList />
      //   </Route>
        
      //   <Route path="/folder/:folderId">
      //     <Test />
      //   </Route>
      // </Switch>
      //   </div>
  );
}

export default App;
