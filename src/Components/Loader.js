import React, { useState, useEffect } from "react";
import CircularProgress from "@mui/material/CircularProgress";
import LoadingOverlay from 'react-loading-overlay-ts';
import { CircleSpinnerOverlay, FerrisWheelSpinner,DartsSpinner  } from 'react-spinner-overlay'
import Box from "@mui/material/Box";

export default function App() {
  const [isFetching, setIsFetching] = useState(true);

  // useEffect(() => {
  //   setTimeout(function () {
  //     console.log("Delayed for 5 second.");
  //     setIsFetching(false);
  //   }, 500000);
  // }, []);

  if (isFetching) {
    return (
      <LoadingOverlay
      active={isFetching}
      spinner={<CircleSpinnerOverlay color="#4D8DEA" />}
    >
     </LoadingOverlay>
    );
  }

  // return <div>content....</div>;
}
