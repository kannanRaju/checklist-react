import React, { useEffect, useState } from 'react';
import axios from 'axios';
import PermContactCalendarIcon from '@mui/icons-material/PermContactCalendar';
import MarkChatReadSharpIcon from '@mui/icons-material/MarkChatReadSharp';
import AddCircleSharpIcon from '@mui/icons-material/AddCircleSharp';
import MoreVertSharpIcon from '@mui/icons-material/MoreVertSharp';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
//import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import moment from 'moment';
import { Link } from "react-router-dom";
import Toast from './SuccessToast';
import ErrorToast from './ErrorToast';
import edit from '../images/edit_black.svg';
import del from '../images/delete_black.svg';
import foldericon from '../images/folder_black.svg';
import addfoldericon from '../images/add_circle_black.svg';
import listcount from '../images/library_add_check_black.svg';
import calendericon from '../images/calendar_month_black.svg';
import logo from '../images/logo.png';
import left from '../images/chevron_left_black.svg';
import right from '../images/chevron_right_black.svg';
import vertical from '../images/more-vert.svg';
import DeleteIcon from '@mui/icons-material/Delete';
import * as api from '../service/api.js';
import Loader from './Loader';
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";

import Pagination from "react-js-pagination";

//import {Link} from 'react-router-dom';
const FolderList = () => {
    const [folderList, setFolderList] = useState([]);
    const [totalCount, settotalCount] = useState();
    const [open, setOpen] = React.useState(false);
    const [editOpen, setEditOpen] = React.useState(false);
    const [deleteOpen, setDeleteOpen] = React.useState(false);

    const [FolderId, setFolderId] = useState('');
    const [userData,setUserData]=useState({UserId:'12',RoleId:'1',UserType:'admin',AccessToken:'3GFCY5DBQRJXUOAL1IN8P7WZS69VKM2EHT'});   
    const [formData, setformData] = useState({
        name: ''
    });
    const [reason, setreason] = useState('');
    const [toastFlag, settoastFlag] = useState(false);
    const [errorToastFlag, seterrorToastFlag] = useState(false);
    const [isFetching, setIsFetching] = useState(true);
    const [activePage, setactivePage] = useState(1);
    //const history = useHistory();
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    //   const handleClickEditOpen = () => {
    //     setEditOpen(true);
    //   };

    const handleCloseEdit = () => {
        setEditOpen(false);
    };
    const handleDeleteFolder = () => {
        setDeleteOpen(false);
    };
    

    //const navigate = useNavigate();

    // pagination 
    // const [currentPage, setcurrentPage] = useState(1);
    // const [itemPerPage, setitemPerPage] = useState(8);

    // const [pageNumberLimit, setpageNumberLimit] = useState(5);
    // const [maxPageNumberLimit, setmaxPageNumberLimit] = useState(5);
    // const [minPageNumberLimit, setminPageNumberLimit] = useState(0);

    // const handleClick = (event) => {
    //     setcurrentPage(Number(event.target.id));
    //     setpageNumberLimit(3);
    //     setitemPerPage(8);
    // }
    // const pages = [];

    // for (let i = 1; i <= Math.ceil(folderList.length / itemPerPage); i++) {
    //     pages.push(i);
    // }

    // const indexOfLastItem = currentPage * itemPerPage;
    // const indexOfFirstItem = indexOfLastItem - itemPerPage;
    // const currentItem = folderList.slice(indexOfFirstItem, indexOfLastItem);

    // const renderPageNumbers = pages.map(number => {
    //     if (number < maxPageNumberLimit + 1 && number > minPageNumberLimit) {
    //         return (
    //             <li key={number} id={number} onClick={handleClick} className={currentPage === number ? 'active' : null}>{number}</li>
    //         )
    //     }
    //     else {
    //         return null;
    //     }
    // })

    // const handleNext = () => {
    //     setcurrentPage(currentPage + 1);
    //     if (currentPage + 1 > maxPageNumberLimit) {
    //         setmaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
    //         setminPageNumberLimit(minPageNumberLimit + pageNumberLimit);
    //     }
    // }

    // const handlePrev = () => {
    //     setcurrentPage(currentPage - 1);
    //     if ((currentPage + 1) % pageNumberLimit === 0) {
    //         setmaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
    //         setminPageNumberLimit(minPageNumberLimit - pageNumberLimit);
    //     }
    // }

    //end

    const folders = async (activePage) => {

        try {
            const params = {
                UserId: 12,
                RoleId: 1,
                UserType: 'admin',
                AccessToken: '3GFCY5DBQRJXUOAL1IN8P7WZS69VKM2EHT',
                Page: activePage,
                Limit: 8
            }
            console.log(params);
            setIsFetching(true);
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'folderlist/', params, {
                headers: header
            }).then((response) => {
                setIsFetching(false);
                setFolderList(response.data[0].FolderDetails);
                settoastFlag(false);
                seterrorToastFlag(false);
                console.log(response);
                settotalCount(response.data[0].TotalCount);
            }).catch((error) => {
                //console.log(error);
            })
        } catch (e) {
            //console.log(e);
        }
    }



    useEffect(() => {
        folders();
    }, []);

    const handle = (e) => {
        const newData = { ...formData }
        newData[e.target.id] = e.target.value;
        setformData(newData);
    }

    const addFolder = () => {
        try {

            const params = {
                FolderName: formData.name,
                UserId: 12
            }
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + '/createfolder/', params, {
                headers: header
            }).then((response) => {
                if (response.data[0].success === 'Y') {
                    setOpen(false);
                    setreason(response.data[0].Reason);
                    settoastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);

                }
                else {
                    setOpen(false);
                    setreason(response.data[0].Reason);
                    seterrorToastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);
                }
                console.log(response);
            }).catch((error) => {
                console.log(error);
            })

        } catch (e) {
            console.log(e);
        }
    }

    const deletePopup = (id) => {
        setDeleteOpen(true);
        setFolderId(id);
    }
    //deleteItem
    const deleteFolder = (id) => {
        try {

            const params = {
                UserId: 12,
                RoleId: 1,
                UserType: 'admin',
                AccessToken: '3GFCY5DBQRJXUOAL1IN8P7WZS69VKM2EHT',
                FolderId: FolderId
            }
           
            let headers = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'deletefolderandchecklist/', params, {
                headers: headers
            }).then((response) => {

                if (response.data[0].success === 'Y') {
                    setOpen(false);
                    setreason(response.data[0].Reason);
                    settoastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);

                }
                else {
                    setOpen(false);
                    setreason(response.data[0].Reason);
                    seterrorToastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
    }

    //getFoldername and FolderId
    const editFolder = (id, name) => {
        setFolderId(id);
        const newData = { ...formData }
        newData['name'] = name;
        setformData(newData);
        setEditOpen(true);
    }

    const handlePageChange = (e) =>{
        setactivePage(e);
        folders(e);
    }

    //updateFolder
    const updateFolder = async () => {
        try {

            const params = {
                AliasValues: {
                    folder_name: formData.name
                },
                AliasStatus: 'Update',
                AliasKey: 'FOLDERS',
                AliasPrimaryKey: FolderId,
                UserId: 12,
                RoleId: 1,
                UserType: 'admin',
                AccessToken: '3GFCY5DBQRJXUOAL1IN8P7WZS69VKM2EHT'
            }

            let headers = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'get_masterlistModifyAction/', params, {
                headers: headers
            }).then((response) => {
                console.log(response);
                if (response.data.success === 'Y') {
                    setEditOpen(false);
                    setreason(response.data.Reason);
                    settoastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);

                }
                else {
                    setEditOpen(false);
                    setreason(response.data.Reason);
                    seterrorToastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
    }


    return (

        <div>
            
            {/* <RotateLoader color="#36d7b7" loading /> */}
            <div className="section">
                <div className="header_sec">
                    {toastFlag && <Toast headline={reason} />}
                    {errorToastFlag && <ErrorToast headline={reason} />}
                    <img src={logo} alt="Heptagon Logo" className='logo' />
                </div>
                <div className='body_sec'>
                    <div className='mb-3'>
                        <span><img src={foldericon} alt='Folder List' /><h5 className='title'>Folder List</h5></span>
                        <span className='new_folder'><img src={addfoldericon} alt="Add Folder" onClick={handleClickOpen} /></span>
                    </div>

                    <div className='list_row'>

                        {
                            folderList.map((row) => (


                                <div className='folders'>

                                    <div className='folders_list' headlineid={row.FolderId} key={row.FolderId}>
                                        <div className="header"></div>
                                        <div className='three_dots'>
                                            <div className=''>
                                                <img src={edit} alt='Edit Folder' className="border-icon" onClick={() => editFolder(row.FolderId, row.FolderName)} /> <img src={del} className="border-icon" onClick={() => deletePopup(row.FolderId)} alt='Delete Folder' />
                                            </div>
                                        </div>
                                        <Link to={`/checklist/${row.FolderId}`} state={{ commonData: userData}} className="folder-id" >
                                            <div className='name_of_folder'>
                                                <div className='char_at'>{row.FolderName.charAt(0).toUpperCase()}</div>
                                                <div className='folder_name'>{row.FolderName.charAt(0).toUpperCase() + row.FolderName.substring(1)}</div>
                                            </div>
                                            <div className='folder-bottom'>
                                                <div className='items_count'><img src={listcount} />&nbsp;&nbsp;<span className='item-date'>{row.TotalCheckLists + ' items'}</span></div>
                                                <div className='date_icon'>
                                                    <img src={calendericon} />&nbsp;&nbsp;<span className='item-date'>{moment(row.LastUpdate).format("DD/MM/YYYY")}</span>
                                                </div>
                                            </div>

                                        </Link>
                                    </div>

                                </div>


                            ))

                        }
                    </div>
                    <div className='pagination pagination-margin'>
                    <Pagination
                        activePage={activePage}
                        itemsCountPerPage={8}
                        totalItemsCount={totalCount}
                        // pageRangeDisplayed={3}
                        onChange={handlePageChange}
                        activeClass= "active"
                        activeLinkClass="activeLink"
                        />
                       
                    </div>
                </div>

            </div>


            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Add Folder</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Folder Name"
                        type="text"
                        fullWidth
                        name="name"
                        variant="standard"
                        onChange={(e) => handle(e)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} className='button-cancel'>Cancel</Button>
                    <Button onClick={addFolder} className='button-primary'>Add</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={editOpen} onClose={handleCloseEdit}>
                <DialogTitle>Edit Folder</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Folder Name"
                        type="text"
                        fullWidth
                        name="name"
                        defaultValue={formData.name}
                        variant="standard"
                        onChange={(e) => handle(e)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseEdit} className='button-cancel'>Cancel</Button>
                    <Button onClick={updateFolder} className='button-primary'>Update</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={deleteOpen} onClose={handleDeleteFolder}>
                <DialogTitle>Delete Folder</DialogTitle>
                <DialogContent>
                Are you sure you want to delete this folder?
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDeleteFolder} className='button-cancel'>No</Button>
                    <Button onClick={() => deleteFolder()} className='button-primary'>Yes</Button>
                </DialogActions>
            </Dialog>
           { isFetching && <Loader /> }
        </div>

        

    );

    
}

export default FolderList;