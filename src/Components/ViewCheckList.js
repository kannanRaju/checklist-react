import React, { useState, useEffect, useRef } from 'react';
import { useLocation } from 'react-router-dom';
import { useParams } from "react-router-dom"
import axios from 'axios';
import taskImg from '../images/pending_actions.svg';
import pendingImg from '../images/assignment_late.svg';
import check_circle from '../images/check_circle.svg';
import view_log from '../images/comment_black_18dp.svg';
import preview from '../images/preview_icon.svg';
import moment from 'moment';

import ProgressBar from './Progress';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import left from '../images/chevron_left_black.svg';
import right from '../images/chevron_right_black.svg';
import Loader from './Loader';
import logo from '../images/logo.png';
import Navigation from './Navigation';
import TextField from '@mui/material/TextField';
import FileUpload from "react-material-file-upload";
// import RichTextEditor from 'react-rte';
import { useForm } from 'react-hook-form';
// import { ContentState, convertToRaw, convertFromHTML, createWithContent } from 'draft-js';
// import { Editor, EditorState } from "react-draft-wysiwyg";
// import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import {CKEditor} from "@ckeditor/ckeditor5-react";
import Editor  from "@ckeditor/ckeditor5-build-classic";
import Pagination from "react-js-pagination";

const ViewCheckList = () => {
    
    const [TaskList, setTaskList] = useState([]);
    const [CheckListDetail, setCheckListDetail] = useState([]);
    const [ProgressDetail, setProgressDetail] = useState([]);
    const [closeChecklist, setCloseChecklist] = useState();
    const [StatusList, setStatusList] = useState([]);

    const [taskId, setTaskId] = useState();
    const [taskName, setTaskName] = useState();
    const [assigneeName, setAssigneeName] = useState();

    const [viewComments, setViewComments] = React.useState(false);
    const [commentsList, setCommentsList] = useState([]);

    const [statusId, setStatusId] = React.useState();
    const [comments, setComments] = React.useState();
    
    const [statusPopup, setStatusPopup] = useState(false);
    const [closeChecklistOpen, setCloseChecklistOpen] = useState(false);
    const [isopenFlag, setIsOpenFlag] = useState(false);
    
    const [files, setFiles] = useState([]);
    const { register, handleSubmit } = useForm();
    const [reason, setreason] = useState('');
    const [toastFlag, settoastFlag] = useState(false);
    const [errorToastFlag, seterrorToastFlag] = useState(false);
    const [checklistMessage, setChecklistMessage] = useState();
    
    const divRef = useRef(null);
    const [totalCount, settotalCount] = useState();
    const [activePage, setactivePage] = useState(1);
    const [contentState, setContentState] = useState(); // ContentState JSON
    const [htmlContent, setHTMLContent] = useState(); 
    const [showPagination, setshowPagination] = useState(false);
    
    // pagination 
    // const [currentPage, setcurrentPage] = useState(1);
    // const [itemPerPage, setitemPerPage] = useState(8);

    // const [pageNumberLimit, setpageNumberLimit] = useState(5);
    // const [maxPageNumberLimit, setmaxPageNumberLimit] = useState(5);
    // const [minPageNumberLimit, setminPageNumberLimit] = useState(0);

     const [isFetching, setIsFetching] = useState(true);
    // // const [showResults, setShowResults] = React.useState(false);
    const handleCloseComments = () => {
        setViewComments(false);
    };
    const handleCloseCommentsAttachments = () => {
        setStatusPopup(false);
       
    };
    const handleCloseChecklist = () => {
        setCloseChecklistOpen(false);
        setIsOpenFlag(false);
    };
    // const handleClick = (event) => {
    //     setcurrentPage(Number(event.target.id));
    //     setpageNumberLimit(3);
    //     setitemPerPage(8);
    // }
          
    // const pages = [];

    // for (let i = 1; i <= Math.ceil(TaskList.length / itemPerPage); i++) {
    //     pages.push(i);
    // }

    // const indexOfLastItem = currentPage * itemPerPage;
    // const indexOfFirstItem = indexOfLastItem - itemPerPage;
    // const ViewChecklistItem = TaskList.slice(indexOfFirstItem, indexOfLastItem);
   
    // const renderPageNumbers = pages.map(number => {
    //     if (number < maxPageNumberLimit + 1 && number > minPageNumberLimit) {
    //         return (
    //             <li key={number} id={number} onClick={handleClick} className={currentPage === number ? 'active' : null}>{number}</li>
    //         )
    //     }
    //     else {
    //         return null;
    //     }
    // })

    // const handleNext = () => {
    //     setcurrentPage(currentPage + 1);
    //     if (currentPage + 1 > maxPageNumberLimit) {
    //         setmaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
    //         setminPageNumberLimit(minPageNumberLimit + pageNumberLimit);
    //     }
    // }

    // const handlePrev = () => {
    //     setcurrentPage(currentPage - 1);
    //     if ((currentPage + 1) % pageNumberLimit === 0) {
    //         setmaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
    //         setminPageNumberLimit(minPageNumberLimit - pageNumberLimit);
    //     }
    // }

    //end

    //const [age, setAge] = React.useState('');
    // const [open, setOpen] = React.useState(false);
    const [open, setOpen] = useState(() => {
        return Array(TaskList.length).fill(false); // this will initialize the open useState hook  based on the length of your ViewChecklistItem so open = [false,false,false,false] if you have for example 4 items in the list
    });
    const [status, setStatus] = useState(() => {
        return Array(TaskList.length).fill(''); // age = ['','','','']
    });

    //   const handleChange = (event: SelectChangeEvent<typeof age>) => {
    //     this.setAge(event.target.value);
    //   };

    //   const handleClose = () => {
    //     setOpen(false);
    //   };

    //   const handleOpen = () => {
    //     setOpen(true);
    //   };
  
   
    const handleClose = (index: number): void => {
        var openArray: boolean[] = [...open];
        openArray[index] = false;
        setOpen(openArray);
    };

    const handleOpen = (index: number): void => {
        var openArray: boolean[] = [...open];
        openArray[index] = true;
        setOpen(openArray);
    };

    const handleChange = (event: SelectChangeEvent<typeof status>, index: number) => {
        var statusArray: string[] = [...status];
        statusArray[index] = (event.target.value);
        console.log(statusArray[index]);
        setStatus(statusArray);
    };

    /* for state get data from link */
    const location = useLocation();
    const { commonData } = location.state;
    /* useparams for get id from URL */
    let paramsId = useParams()
    const [navigate, setNavigate] = useState([{folderId:paramsId.folderId, pageName:"viewChecklist"}]);
   
    const folders = async (activePage) => {

        try {
            const params = {
                CheckListId: paramsId.checklistId,
                UserId: commonData.UserId,
                RoleId: commonData.RoleId,
                UserType: commonData.UserType,
                AccessToken: commonData.AccessToken,
                Page: activePage,
                Limit: 10
            }
            setIsFetching(true);
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'viewchecklistdetails/', params, {
                headers: header
            }).then((response) => {
                console.log(response);
                setTaskList(response.data[0].TaskDetails);
                setCheckListDetail(response.data[0].CheckListDetails[0]);
                setProgressDetail(response.data[0].ProgressDetails[0]);
                setCloseChecklist(response.data[0].CloseChecklist);
                setIsFetching(false);
                settotalCount(response.data[0].TotalCount);
                response.data[0].TotalCount > 10 ?  setshowPagination(true) :  setshowPagination(false);
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }



        try {
            const params = {
                UserId: commonData.UserId,
            }
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'statuslist/', params, {
                headers: header
            }).then((response) => {
                console.log(response);
                setStatusList(response.data[0].StatusList);

            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
    }

    const viewLogs = (TaskId, TaskName, AssigneeName) => {
        setTaskId(TaskId);
        setTaskName(TaskName);
        setAssigneeName(AssigneeName);
        setViewComments(true);

        try {
            const params = {
                UserId: commonData.UserId,
                RoleId: commonData.RoleId,
                UserType: commonData.UserType,
                AccessToken: commonData.AccessToken,
                TaskId : TaskId
            }
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'viewuploadeddocuments/', params, {
                headers: header
            }).then((response) => {
                console.log(response);
                setCommentsList(response.data[0].AttachmentsAndComments);

            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
         
    }

    
    const closeChecklistEmail = () => {
       
        try {
            const params = {
                UserId: commonData.UserId,
                RoleId: commonData.RoleId,
                UserType: commonData.UserType,
                AccessToken: commonData.AccessToken,
                CheckListId: paramsId.checklistId,
                EmailContent: checklistMessage
            }
            
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'closechecklist/', params, {
                headers: header
            }).then((response) => {
                if (response.data.success === 'Y') {
                    setCloseChecklistOpen(false);
                    setreason(response.data.Reason);
                    settoastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);

                }
                else {
                    setCloseChecklistOpen(false);
                    setreason(response.data.Reason);
                    seterrorToastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);
                }

            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
         
    }
    const onSubmit = async (data) => {
        const formData = new FormData();
        formData.append("Attachments", data.file[0]);
        formData.append("CheckListId", paramsId.checklistId);
        formData.append("UserId", commonData.UserId);
        formData.append("RoleId", commonData.RoleId);
        formData.append("UserType", commonData.UserType);
        formData.append("AccessToken", commonData.AccessToken);
        formData.append("Comments", comments);
        formData.append("TaskId", taskId);
        formData.append("status_id", statusId);
        
       
        try {
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'commentsattachments/', formData, {
                headers: header
            }).then((response) => {
                console.log(response);
                if (response.data.success === 'Y') {
                    setStatusPopup(false);
                    setreason(response.data.Reason);
                    settoastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);

                }
                else {
                    setStatusPopup(false);
                    setreason(response.data.Reason);
                    seterrorToastFlag(true);
                    setTimeout(() => {
                        folders();
                    }, 4000);
                }
                
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
    }

    const closeChecklistPopup = () => {
        setCloseChecklistOpen(true);
        setIsOpenFlag(true);
        setHTMLContent(divRef.current.innerHTML);
    }

 
    
    useEffect(() => {
        folders();
    }, []);

    const handlePageChange = (e) =>{
        setactivePage(e);
        folders(e);
    }
    


    return (
        <div>
       
            <div className="section">
                <div className="header_sec">
                   
                    <img src={logo} alt="Heptagon Logo" className='logo' />
                </div>
                <div className='body_sec chklist_padding'>
                    <div className='row'>
                        <div className='col-lg-6 mt-3'>
                        <Navigation data= {navigate} />
                        </div>
                    </div>
                    <div class="row">
                        <div className='col-lg-5'>
                            <div className='checklistdetails'>
                                <div class="row">
                                    <div className='col-lg-3 tasklabel'>CheckList : </div>
                                    <div className='col-lg-9 taskchecklistname'>{CheckListDetail.ChecklistName}</div>
                                </div>
                                <div class="row">
                                    <div className='col-lg-3 tasklabel'>Project : </div>
                                    <div className='col-lg-9 taskchecklistname'>{CheckListDetail.ProjectName}</div>
                                </div>
                                <div class="row">
                                    <div className='col-lg-3 tasklabel'>Owner : </div>
                                    <div className='col-lg-9 taskchecklistname'>{CheckListDetail.OwnerName}</div>
                                </div>
                            </div>
                        </div>
                        <div className='col-lg-7'>
                            <div className="progressdetail">
                                <div class="">
                                    <div className='progress-task'><span className='total-task'><img src={taskImg} alt="Total Task" />&nbsp;&nbsp;Total Task - {ProgressDetail.TotalTasks}</span></div>
                                    <div className='progress-task'><span className='pending-task'><img src={pendingImg} alt="Open Task" />&nbsp;&nbsp;Open Task - {ProgressDetail.Open}</span></div>
                                    <div className='progress-task'><span className='completed-task'><img src={check_circle} alt="Completed" />&nbsp;&nbsp;Completed - {ProgressDetail.Completed}</span></div>
                                    
                                    {
                                        closeChecklist === 'N' ? (<div className='progress-task'><a href="javascript:void(0)" className='close-chk' onClick={()=> closeChecklistPopup()}>Close Checklist</a></div>) : (<div className='progress-task'><span className='closed-chk'> 
                                        Checklist Closed</span></div>)
                                        
                                   
                                }
                                   
                                </div>
                                <div className='view-percentage'>
                                    <ProgressBar percentage={ProgressDetail.PercentageCompleted} />
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    <div className='row'>
                        <div className='col-lg-12'>
                            <table id="checklist">
                                <tr>
                                    <th>Task List</th>
                                    <th>Assignee</th>
                                    <th>Status</th>
                                    <th>Comments</th>
                                </tr>

                                {

                                    TaskList.map((row, index) => (

                                        <tr key={row.TaskId}>
                                            <td width="45%">{row.TaskName}</td>
                                            <td>{row.AssigneeName}</td>
                                            <td>

                                                <FormControl sx={{ m: 1, minWidth: 120 }}>
                                                    <InputLabel id="demo-controlled-open-select-label">Status</InputLabel>
                                                    <Select
                                                        className={"copy "+(row.AllowStatusChange === 'Y' ? 'allowed' : 'not-allowed')}
                                                        labelId="demo-controlled-open-select-label"
                                                        id="demo-controlled-open-select"
                                                        open={open[index]}
                                                        onClose={() => { handleClose(index) }}
                                                        onOpen={() => { handleOpen(index) }}
                                                        value={status[index] || row.StatusId}
                                                        label="Status"
                                                        onChange={(event, newValue) => { 
                                                            setStatusId(event.target.value);
                                                            setTaskId(row.TaskId);
                                                            setStatusPopup(true);
                                                        }}
                                                    >
                                                        {StatusList?.map(status => {
                                                            return (

                                                                <MenuItem value={status.StatusId}>{status.Status}</MenuItem>
                                                            );
                                                        })}
                                                    </Select>
                                                </FormControl>
                                            </td>
                                            <td><a href="javascript:;" onClick={() => viewLogs(row.TaskId, row.TaskName, row.AssigneeName)} className='view-log'><img src={view_log} />&nbsp;&nbsp;View Log</a></td>
                                        </tr>

                                    ))

                                }


                            </table>
                        </div>
                    </div><br></br>
                   {showPagination && <div className='pagination'>
                    <Pagination
                        activePage={activePage}
                        itemsCountPerPage={8}
                        totalItemsCount={totalCount}
                        // pageRangeDisplayed={3}
                        onChange={handlePageChange}
                        activeClass= "active"
                        activeLinkClass="activeLink"
                        />
                    </div>
                    }
                    <div className={'row '+(isopenFlag == true ? 'table_show' : 'table_hide')} ref={divRef}>
                    <div class="row">
                        <table>
                            <tr>
                                <th>Checklist Name</th>
                                <th>Project Name</th>
                                <th>Owner Name</th>
                            </tr>
                            <tr>
                                <td>{CheckListDetail.ChecklistName}</td>
                                <td>{CheckListDetail.ProjectName}</td>
                                <td>{CheckListDetail.OwnerName}</td>
                            </tr>
                        </table>
                    </div>
                        <div className='col-lg-12'>
                            <table id="checklist">
                                <tr>
                                    <th>Task List</th>
                                    <th>Assignee</th>
                                    <th>Status</th>
                                </tr>

                                {

                                    TaskList.map((row, index) => (

                                        <tr key={row.TaskId}>
                                            <td width="45%">{row.TaskName}</td>
                                            <td>{row.AssigneeName}</td>
                                            <td> {row.Status} </td>
                                           
                                        </tr>

                                    ))

                                }
                            </table>
                        </div>
                    </div>
                    
                    <Dialog open={viewComments} onClose={handleCloseComments} id="view-checklist-comments">
                        <DialogTitle>{taskName} <CancelRoundedIcon className='comments-close' onClick={handleCloseComments} /></DialogTitle>
                        <DialogContent>
                            {
                                commentsList.map((row, index) => (
                            <div className='comments-list-bg'>
                                <div class="row">
                                    <div class="col-lg-8"> <span className='char_at_comments'>{row.CommentedBy.charAt(0).toUpperCase()}</span>&nbsp;&nbsp;<span className="commented-by">{row.CommentedBy}</span> </div>
                                    <div class="col-lg-4 text-center">{moment(row.CreatedDate).format("DD/MM/YYYY hh:ss")}
                                    </div>
                                    <div className='col-lg-12 comments'>{row.Comments}<div className='clear'><img src={row.Attachments} className="file-attachemnt" /></div></div>
                                    <div className='col-lg-12'></div>
                                </div>
                            </div>
                        
                                ))
                            }
                        </DialogContent>
                        
                    </Dialog>

                    <Dialog open={closeChecklistOpen} id="view-close-checklist">
                        <DialogTitle>Close Checklist</DialogTitle>
                        <DialogContent className='mt-3'>
                        <textarea id="textEdit" name="tname" onChange={(e) => setChecklistMessage(e.target.value)} placeholder="Do you want to something write?"></textarea>
                            <CKEditor editor={Editor} data={htmlContent} />
                        </DialogContent>
                        <DialogActions>
                        <Button onClick={handleCloseChecklist} className='button-cancel'>Cancel</Button>
                        <Button className='button-primary' onClick={() => closeChecklistEmail()} >Send Email</Button>
                    </DialogActions>
                    </Dialog>
                    

                    <Dialog open={statusPopup} onClose={handleCloseCommentsAttachments} id="view-checklist-comments">
                        <DialogTitle>Comments and Attachments<CancelRoundedIcon className='comments-close' onClick={handleCloseCommentsAttachments} /></DialogTitle>
                        <DialogContent>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className='upload-card mt-3'>
                                <div className='row'>
                                    <div className='col-lg-12'>
                                    <TextField fullWidth label="Comments" id="add-comments" placeholder='Enter your comments' onChange={e => setComments(e.target.value)} autoComplete='off'/>
                                    </div>
                                     <FileUpload value={files} onChange={setFiles}   /> 
                                    <input type="file" {...register("file")} className="filehidden" />

                                </div>
                                {/* <a href="javascript:;" onClick={() => postComments()} className='post-comments'>Post your comments</a> */}
                                
                                <input type="submit" className='post-comments'/>
                            </div>
                            </form>
                        </DialogContent>
                        
                    </Dialog>
                    { isFetching && <Loader /> } 
                  
            </div>
        </div>
       
    </div>
    
    );
}




export default ViewCheckList;