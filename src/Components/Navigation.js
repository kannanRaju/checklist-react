import React from 'react';
// import Snackbar from '@mui/material/Snackbar';
// import MuiAlert from '@mui/material/Alert';

import { emphasize, styled } from '@mui/material/styles';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Chip from '@mui/material/Chip';

const Navigation = (props) => {

const folderId = props.data[0].folderId;

const StyledBreadcrumb = styled(Chip)(({ theme }) => {
  const backgroundColor =
    theme.palette.mode === 'light'
      ? theme.palette.grey[100]
      : theme.palette.grey[800];
  return {
    backgroundColor,
    height: theme.spacing(3),
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: emphasize(backgroundColor, 0.06),
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(backgroundColor, 0.12),
    },
  };
});



    return ( 
      
        <div>
            <div role="presentation">
              <Breadcrumbs aria-label="breadcrumb">
                <StyledBreadcrumb
                  component="a"
                  href="/"
                  label="Folder List"
                />
                 
                 <StyledBreadcrumb component="a" href={`/checklist/${folderId}`} label="Checklist" /> 
                  
                  { props.data[0].pageName === 'addChecklist' && 
                    <StyledBreadcrumb component="a" href="#" label="Add Checklist" />
                  }

                  { props.data[0].pageName === 'editChecklist' && 
                    <StyledBreadcrumb component="a" href="#" label="Edit Checklist" />
                  }

                  { props.data[0].pageName === 'viewChecklist' && 
                    <StyledBreadcrumb component="a" href="#" label="View Checklist" />
                  }

                {/* if(props.pageName === 'addChecklist') {
                    <StyledBreadcrumb component="a" href="#" label="Add Checklist" />
                }
                 else if(props.pageName === 'editChecklist') {
                    <StyledBreadcrumb component="a" href="#" label="Edit Checklist" />
                } */}
                {/* <StyledBreadcrumb
                  label="Checklist"
                /> */}
              </Breadcrumbs>
            </div>
        </div>

     );
}
 
export default Navigation;