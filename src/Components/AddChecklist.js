
import React, { useState, useEffect } from 'react';
import {useLocation} from 'react-router-dom';
import { useParams } from "react-router-dom"
import logo from '../images/logo.png';
import './addchecklist.css';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import axios from 'axios';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Loader from './Loader';
import Toast from './SuccessToast';
import ErrorToast from './ErrorToast';
import Navigation from './Navigation';
const AddChecklist = () => {

const top100Films = [
   
    { title: 'City of God', year: 2002 },
    { title: 'Se7en', year: 1995 },
    { title: 'The Silence of the Lambs', year: 1991 },
    { title: "It's a Wonderful Life", year: 1946 },
    { title: 'Life Is Beautiful', year: 1997 },
    { title: 'The Usual Suspects', year: 1995 },
    { title: 'Léon: The Professional', year: 1994 },
    { title: 'Spirited Away', year: 2001 },
    { title: 'Saving Private Ryan', year: 1998 },
    { title: 'Once Upon a Time in the West', year: 1968 },
    { title: 'American History X', year: 1998 },
    { title: 'Interstellar', year: 2014 },
    { title: 'Casablanca', year: 1942 },
    { title: 'City Lights', year: 1931 },
    { title: 'Psycho', year: 1960 },
    { title: 'The Green Mile', year: 1999 },
    { title: 'The Intouchables', year: 2011 },
    { title: 'Modern Times', year: 1936 },
    { title: 'Raiders of the Lost Ark', year: 1981 },
    { title: 'Rear Window', year: 1954 },
    { title: 'The Pianist', year: 2002 },
    { title: 'The Departed', year: 2006 },
  ];
  const [userData,setUserData]=useState({UserId:'12',RoleId:'1',UserType:'admin',AccessToken:'3GFCY5DBQRJXUOAL1IN8P7WZS69VKM2EHT'}); 
   /* useparams for get id from URL */
   let paramsId = useParams();
   const [navigate, setNavigate] = useState([{folderId:paramsId.folderId, pageName:"addChecklist"}]);
   console.log(navigate);
  const [projectList, setProjectList] = useState([]);
  const [checkListId, setCheckListId] = useState([]);
  const [ownerList, setOwnerList] = useState([]);
  const [checklistOwnerId, setChecklistOwner] = useState('');
  const [checkListName, setCheckListName] = useState('');
  const [checklistProjectId, setChecklistProject] = useState([])
  const [formValues, setFormValues] = useState([{ taskname: "", assignee : ""}])
  const [isFetching, setIsFetching] = useState(true);
  const [fields, setFields] = useState([]);
  const [toastFlag, settoastFlag] = useState(false);
  const [errorToastFlag, seterrorToastFlag] = useState(false);
  const [reason, setreason] = useState('');
  const userList = async () => {
    try {
        const params = {
            AliasKey:'USERDROP'
        }
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'get_masterlistAction/', params, {
            headers: header
        }).then((response) => {
            setOwnerList(response.data.USERDROP);
        }).catch((error) => {
            //console.log(error);
        })
    } catch (e) {
       // console.log(e);
    }
  }

  const projectlist = async () => {
    try {
        const params = {
            AliasKey:'PROJECTDROP'
        }
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'get_masterlistAction/', params, {
            headers: header
        }).then((response) => {
            setProjectList(response.data.PROJECTDROP);
        }).catch((error) => {
            //console.log(error);
        })
    } catch (e) {
       // console.log(e);
    }
  }
  

    let addFormFields = () => {
        setFormValues([...formValues, { tskname: "", assignee: "" }])
      }
    
    let removeFormFields = (i) => {
        let newFormValues = [...formValues];
        newFormValues.splice(i, 1);
        setFormValues(newFormValues);
    }

    let handleChangeTask = (i, event) => {
        const { name, value } = event.target;
        const list = [...formValues];
        list[i][name] = value;
        setFormValues(list);
    }


    /* placeholder style */
    const useStyles = makeStyles({
        customTextField: {
          "& input::placeholder": {
            fontSize: "15px"
          }
        }
      })
    const classes = useStyles();

    useEffect(() => {
        projectlist();
        userList();
        
    }, []);
    

   /* for state get data from link */
//    const location = useLocation();
//    const { commonUserData} = location.state;
  
  const createCheckList = async (ownerId) => {
    try {
        const params = {
            UserId: userData.UserId,
            RoleId: userData.RoleId,
            UserType: userData.UserType,
            AccessToken: userData.AccessToken,
            CheckListDetails :{ChecklistName:checkListName, FolderId:paramsId.folderId,  ProjectId:checklistProjectId, OwnerId : ownerId}
        }
        setIsFetching(true);
    
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'createchecklist/', params, {
            headers: header
        }).then((response) => {
            if (response.data[0].success === 'Y') {
                setCheckListId(response.data[0].CheckListId);
                setreason(response.data[0].Reason);
                settoastFlag(true);
            }
            else {
                setreason(response.data[0].Reason);
                seterrorToastFlag(true);
            }
            console.log(response);
            //setIsFetching(false);
            
        }).catch((error) => {
            console.log(error);
        })
    } catch (e) {
        console.log(e);
    }
  }


    let saveFieldsDetails = () => {
        try {
            const params = {
                FolderId: paramsId.folderId,  
                UserId: userData.UserId,
                RoleId: userData.RoleId,
                UserType: userData.UserType,
                AccessToken: userData.AccessToken,
                CheckListId: paramsId.checklistId,
                TaskDetails: formValues,
            }
           
            setIsFetching(true);
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'createtasks/', params, {
                headers: header
            }).then((response) => {
                if (response.data[0].success === 'Y') {
                    setreason(response.data[0].Reason);
                    settoastFlag(true);
                }
                else {
                    setreason(response.data[0].Reason);
                    seterrorToastFlag(true);
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
    }

    return ( 
        <div className='add-chk-bg'>
             {toastFlag && <Toast headline={reason} />}
            {errorToastFlag && <ErrorToast headline={reason} />}
            <div className="section">
                <div className="header_sec">
                   
                    <img src={logo} alt="Heptagon Logo" className='logo' />
                </div>
                <div className='body_sec'>
                    <div className='row'>
                        <div className='col-lg-8'>
                            <Navigation data={navigate} />
                        </div>
                    </div>
                    <div className="row">
                   
                        <div className="col-lg-12 checklist-title mb-3"><h5 className="add_checklist_here">Add Checklist Here</h5></div>
                        <div className='col-lg-4 mt-2'>
                            <div>
                                <TextField
                                id="outlined-error"
                                label="Checklist Name"
                                defaultValue=""
                                onChange={e => setCheckListName(e.target.value)}
                                />
                            
                            </div>
                        </div>
                        <div className='col-lg-4 mt-2'>
                            <Autocomplete
                                multiple
                                limitTags={2}
                                id="tags-outlined"
                                options={projectList}
                                getOptionLabel={(option) => option.text}
                                filterSelectedOptions
                                onChange={(event, newValue) => {
                                    setChecklistProject(newValue[0].id);
                                    console.log(newValue);
                                  }}
                                  disableClearable={true}
                                renderInput={(params) => (
                                <TextField
                                
                                    {...params}
                                    label="Select Project"
                                    placeholder="Select Project"
                                    
                                />
                                
                                )}
                            />
                        </div>
                        <div className='col-lg-4 mt-2'>
                            <Autocomplete
                                multiple
                                key={(option) => option.id}
                                limitTags={2}
                                id="tags-outlined1"
                                options={ownerList}
                                getOptionLabel={(option) => option.text}
                                // defaultValue={[top100Films[13]]}
                                filterSelectedOptions
                                onChange={(event, newValue) => {
                                    //setChecklistOwner(newValue[0].id);
                                    console.log(newValue[0].id);
                                    createCheckList(newValue[0].id);
                                  }}
                                disableClearable={true}
                                renderInput={(params) => (
                                <TextField
                                
                                    {...params}
                                    label="Select Owner"
                                    placeholder="Select Owner"
                                   
                                />
                                )}
                            />
                        </div>
                    </div>
                  
                    <div className='row mt-5 mb-3'>
                        <div className="col-lg-12 checklist-title"><h5 className="add_checklist_here">Add Task Here</h5></div>
                    </div>
                    {formValues.map((element, index) => (
                    <div className="row mt-2"  key={index}>
                   
                        <div className='col-lg-6 mt-2'>
                            <div>
                                <TextField
                                key={index}
                                name="tskname"
                                id="tskname"
                                label="Task Name"
                                defaultValue=""
                                onChange={(e) => handleChangeTask(index, e)}
                                />
                            </div>
                        </div>
                        <div className='col-lg-4 mt-2'  key={index}>
                            <Autocomplete
                               
                                multiple
                                name="assignee"
                                limitTags={2}
                                id="assignee"
                                options={ownerList}
                                getOptionLabel={(option) => option.text}
                                 onChange={(event, newValue) => {
                                    const list = [...formValues];
                                    list[index]['assignee'] = newValue[0].id;
                                    setFormValues(list);
                                  }}
          
                                disableClearable={true}
                                // defaultValue={[top100Films[13]]}
                                filterSelectedOptions
                                renderInput={(params) => (
                                <TextField
                                
                                    {...params}
                                    label="Select Assignee"
                                    placeholder="Select Assignee"
                                    className="auto-label"
                                    
                                />
                                )}
                            />
                        </div>

                        <div className='col-lg-2 mt-3'>
                            <button className='add' onClick={() => addFormFields()}>Add</button>
                             {
                             index ? 
                            <button className='removeformfields' onClick={() => removeFormFields(index)}>Delete</button>
                            : null
                            } 
                        </div>
                       
                      
                    </div>
                     ))}
                     <div className='row mt-3'>
                        <div className='col-lg-6'></div>
                        <div className='col-lg-4'>
                            <button className='submit' onClick={() => saveFieldsDetails()}>Submit</button>
                        </div>
                        <div className='col-lg-2'></div>
                     </div>
                </div>
            </div>
            {/* { isFetching && <Loader /> } */}
        </div>
    
     );
   
}
 
export default AddChecklist;