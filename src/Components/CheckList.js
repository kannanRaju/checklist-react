import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useParams } from "react-router-dom"
import axios from 'axios';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import ProgressBar from './Progress';
import Navigation from './Navigation';
import { Link } from "react-router-dom";
import left from '../images/chevron_left_black.svg';
import right from '../images/chevron_right_black.svg';
import checklist from '../images/checklist_icon.svg';
import logo from '../images/logo.png';
import Loader from './Loader';
import Pagination from "react-js-pagination";
const Test = () => {
    const [checkList, setCheckList] = useState([]);
    const [activePage, setactivePage] = useState(1);
        
    // // pagination 
    // const [currentPage, setcurrentPage] = useState(1);
    // const [itemPerPage, setitemPerPage] = useState(8);

    // const [pageNumberLimit, setpageNumberLimit] = useState(5);
    // const [maxPageNumberLimit, setmaxPageNumberLimit] = useState(5);
    // const [minPageNumberLimit, setminPageNumberLimit] = useState(0);

    const [showResults, setShowResults] = React.useState(false);
    const [isFetching, setIsFetching] = useState(true);
    const [showPagination, setshowPagination] = useState(false);
    const [totalCount, settotalCount] = useState();
    // const handleClick = (event) => {
    //     setcurrentPage(Number(event.target.id));
    //     setpageNumberLimit(3);
    //     setitemPerPage(8);
    // }
    // const pages = [];

    // for (let i = 1; i <= Math.ceil(checkList.length / itemPerPage); i++) {
    //     pages.push(i);
    // }

    // const indexOfLastItem = currentPage * itemPerPage;
    // const indexOfFirstItem = indexOfLastItem - itemPerPage;
    // const CheckListItem = checkList.slice(indexOfFirstItem, indexOfLastItem);

    // const renderPageNumbers = pages.map(number => {
    //     if (number < maxPageNumberLimit + 1 && number > minPageNumberLimit) {
    //         return (
    //             <li key={number} id={number} onClick={handleClick} className={currentPage === number ? 'active' : null}>{number}</li>
    //         )
    //     }
    //     else {
    //         return null;
    //     }
    // })

    // const handleNext = () => {
    //     setcurrentPage(currentPage + 1);
    //     if (currentPage + 1 > maxPageNumberLimit) {
    //         setmaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
    //         setminPageNumberLimit(minPageNumberLimit + pageNumberLimit);
    //     }
    // }

    // const handlePrev = () => {
    //     setcurrentPage(currentPage - 1);
    //     if ((currentPage + 1) % pageNumberLimit === 0) {
    //         setmaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
    //         setminPageNumberLimit(minPageNumberLimit - pageNumberLimit);
    //     }
    // }

    //end

    /* for state get data from link */
    const location = useLocation();
    const { commonData} = location.state;
    /* useparams for get id from URL */
    let paramsId = useParams()
    const [navigate, setNavigate] = useState([{folderId:paramsId.folderId, pageName:"checklist"}]);
  //  console.log(params);
    const folders = async () => {

        try {
            const params = {
                FolderId: paramsId.folderId,
                UserId: commonData.UserId,
                RoleId: commonData.RoleId,
                UserType: commonData.UserType,
                AccessToken: commonData.AccessToken,
                Page: activePage,
                Limit: 8
            }
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'viewchecklists/', params, {
                headers: header
            }).then((response) => {
                response.data[0].TotalCount > 8 ?  setshowPagination(true) :  setshowPagination(false);
                settotalCount(response.data[0].TotalCount);
                setIsFetching(false);
                setCheckList(response.data[0].Checklists);
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
    }

    
    useEffect(() => {
        folders();
    }, []);

    const handlePageChange = (e) =>{
        setactivePage(e);
        folders(e);
    }

     
    return (
        <div>
            <div className="section">
                <div className="header_sec">
                    <img src={logo} alt="Heptagon Logo" className='logo' />
                </div>
                <div className='body_sec chklist_padding'>
                    <div className='row'>
                        <div className='col-lg-8 mt-3'>
                            <Navigation data={navigate}  />
            
                        </div>
                        <div className='col-lg-4'> <div className=''><Link to={`/addchecklist/${paramsId.folderId}`} ><a href="#" className='add_checklist'><img src={checklist} />&nbsp; Add Checklist</a></Link></div></div>
                    </div>
                   
                    <table id="checklist">
                    <tr>
                        <th>Checklist Name</th>
                        <th>Assign To</th>
                        <th>Overall Progress</th>
                        <th>Status</th>
                        <th className='center-align'>Action</th>
                    </tr>
                    
                    {
                    
                    checkList?.map((row) => (

                        <tr className={'tbody '+ (row.CloseFlag === 1 ? 'closedchecklist' : '')} key={row.ChecklistId}>
                            <td width="35%">{row.ChecklistName}</td>
                            {/* <td>{row.ProjectName}</td> */}
                            <td>{row.OwnerName}</td>
                            <td><ProgressBar percentage= {row.ProgressPercentage}/></td>
                            <td><a href="#" className={"status "+ (row.StatusId === 2 ? 'inprogress' : row.StatusId === 4 ? 'closed' : 'others' )}>{row.Status}</a></td>
                            <td className='center-align'><a href="#" id="action-parent">
                        
                                <Tooltip title="More">
                                    <IconButton>
                                        <MoreVertIcon />
                                    </IconButton>
                                </Tooltip>
                                <ul className="action-items">
                                    <li><Link to={`/viewchecklist/${paramsId.folderId}/${row.ChecklistId}`} state={{ commonData: commonData}} >View</Link></li>
                                    <li><Link to={`/editchecklist/${paramsId.folderId}/${row.ChecklistId}`} state={{ commonData: commonData}} >Edit</Link></li>
                                    <li>Delete</li>
                                    <li className={"copy "+(row.CloseFlag == 0 ? 'make-copy-enable' : 'make-copy-disable')}><a href="#">Make a Copy</a></li>
                                </ul>
                            </a> 
                        
                        
                            
                            </td>
                        </tr>

                    ))

                    }
                    
                
                    </table><br></br>
                    { showPagination && <div className='pagination'>
                    <Pagination
                        activePage={activePage}
                        itemsCountPerPage={8}
                        totalItemsCount={totalCount}
                        // pageRangeDisplayed={3}
                        onChange={handlePageChange}
                        activeClass= "active"
                        activeLinkClass="activeLink"
                        />
                                {/* <ul className='page-number'>
                                    <li onClick={handlePrev}><button disabled={currentPage === pages[0] ? true : false}><img src={left} /></button></li>
                                    {renderPageNumbers}
                                    <li onClick={handleNext}><button disabled={currentPage === pages[pages.length - 1] ? true : false}><img src={right} /></button></li>
                                </ul> */}
                            </div>
                    }
                    { isFetching && <Loader /> }
                    </div>
            </div>
        </div>

    );
    

}




export default Test;