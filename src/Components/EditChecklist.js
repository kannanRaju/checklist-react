
import React, { useState, useEffect } from 'react';
import {useLocation} from 'react-router-dom';
import { useParams } from "react-router-dom"
import logo from '../images/logo.png';
import './addchecklist.css';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import axios from 'axios';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Loader from './Loader';
import Toast from './SuccessToast';
import ErrorToast from './ErrorToast';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
//import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Navigation from './Navigation';
import Pagination from "react-js-pagination";

const EditChecklist = () => {

const top100Films = [
   
    { title: 'City of God', year: 2002 },
    { title: 'Se7en', year: 1995 },
    { title: 'The Silence of the Lambs', year: 1991 },
    { title: "It's a Wonderful Life", year: 1946 },
    { title: 'Life Is Beautiful', year: 1997 },
    { title: 'The Usual Suspects', year: 1995 },
    { title: 'Léon: The Professional', year: 1994 },
    { title: 'Spirited Away', year: 2001 },
    { title: 'Saving Private Ryan', year: 1998 },
    { title: 'Once Upon a Time in the West', year: 1968 },
    { title: 'American History X', year: 1998 },
    { title: 'Interstellar', year: 2014 },
    { title: 'Casablanca', year: 1942 },
    { title: 'City Lights', year: 1931 },
    { title: 'Psycho', year: 1960 },
    { title: 'The Green Mile', year: 1999 },
    { title: 'The Intouchables', year: 2011 },
    { title: 'Modern Times', year: 1936 },
    { title: 'Raiders of the Lost Ark', year: 1981 },
    { title: 'Rear Window', year: 1954 },
    { title: 'The Pianist', year: 2002 },
    { title: 'The Departed', year: 2006 },
  ];

  const [userData,setUserData]=useState({UserId:'12',RoleId:'1',UserType:'admin',AccessToken:'3GFCY5DBQRJXUOAL1IN8P7WZS69VKM2EHT'}); 
  const [activePage, setactivePage] = useState(1);
  const [totalCount, settotalCount] = useState();
  const [projectList, setProjectList] = useState([]);
  const [checkListId, setCheckListId] = useState([]);
  const [ownerList, setOwnerList] = useState([]);

  const [checkListName, setCheckListName] = useState('');
   const [checklistProjectId, setChecklistProject] = useState();
   const [ownerId, setChecklistOwner] = useState();
  const [formValues, setFormValues] = useState([]);
  const [isFetching, setIsFetching] = useState(true);
  const [fields, setFields] = useState([]);
  const [toastFlag, settoastFlag] = useState(false);
  const [errorToastFlag, seterrorToastFlag] = useState(false);
  const [reason, setreason] = useState('');
  const [loadProject, setLoadProject] = useState([{ text: "", id : ""}]);
  const [loadOwner, setLoadOwner] = useState([{ text: "", id : ""}]);
  const [loadAssignee, setLoadAssignee] = useState([{ text: "", id : ""}]);
  const [loadAssignee1, setLoadAssignee1] = useState();
  const [open, setOpen] = React.useState(false);
  const [taskId, setTaskId] = React.useState();
  const [showPagination, setshowPagination] = useState(false);
  const userList = async () => {

    try {
        const params = {
            AliasKey:'USERDROP'
        }
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'get_masterlistAction/', params, {
            headers: header
        }).then((response) => {
            setOwnerList(response.data.USERDROP);
        }).catch((error) => {
            //console.log(error);
        })
    } catch (e) {
       // console.log(e);
    }
  }

  const projectlist = async () => {
    try {
        const params = {
            AliasKey:'PROJECTDROP'
        }
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'get_masterlistAction/', params, {
            headers: header
        }).then((response) => {
            setProjectList(response.data.PROJECTDROP);
        }).catch((error) => {
            //console.log(error);
        })
    } catch (e) {
       // console.log(e);
    }
  }
  

    let addFormFields = () => {
        setFormValues([...formValues, { TaskName: "", assignee: "" }])
      }
    const handleClose = () => {
        setOpen(false);
    };
    let removeFormFields = (i, taskid) => {
        
        if(taskid == undefined) {
            let newFormValues = [...formValues];
            newFormValues.splice(i, 1);
            setFormValues(newFormValues);
        }else {
            setOpen(true);
            setTaskId(taskid);
        }
    }

    let handleChangeTask = (i, event) => {
        const { name, value } = event.target;
        const list = [...formValues];
        list[i][name] = value;
        setFormValues(list);
    }

    
    /* placeholder style */
    const useStyles = makeStyles({
        customTextField: {
          "& input::placeholder": {
            fontSize: "15px"
          }
        }
      })
    const classes = useStyles();

    useEffect(() => {
        projectlist();
        userList();
        getCheckListDetails();
        getTaskDetails();
    }, []);

   /* for state get data from link */
//    const location = useLocation();
//    const { commonUserData} = location.state;
   /* useparams for get id from URL */
   let paramsId = useParams();
   const [navigate, setNavigate] = useState([{folderId:paramsId.folderId, pageName:"editChecklist"}]);
  const deleteTask = async () => {
    try {
        const params = {
        
            UserId: userData.UserId,
            RoleId: userData.RoleId,
            UserType: userData.UserType,
            AccessToken: userData.AccessToken,
            TaskId: taskId
        }
        setIsFetching(true);
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'deletefolderandchecklist/', params, {
            headers: header
        }).then((response) => {
            if (response.data[0].success === 'Y') {
                setreason(response.data[0].Reason);
                settoastFlag(true);
            }
            else {
                setreason(response.data[0].Reason);
                seterrorToastFlag(true);
            }
            setOpen(false);
            console.log(response);
            //setIsFetching(false);
            
        }).catch((error) => {
            console.log(error);
        })
    } catch (e) {
        console.log(e);
    }
  }
   const getCheckListDetails = async () => {
    try {
        const params = {
            UserId: userData.UserId,
            CheckListId: paramsId.checklistId
        }
        
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'editchecklist/', params, {
            headers: header
        }).then((response) => {
            if (response.data[0].success === 'Y') {
                const editchecklistName = response.data[0].EditCheckListDetails[0].ChecklistName;
                const editprojectId = response.data[0].EditCheckListDetails[0].ProjectId;
                const editprojectName = response.data[0].EditCheckListDetails[0].ProjectName;
                const editownerId = response.data[0].EditCheckListDetails[0].OwnerId;
                const editownerName = response.data[0].EditCheckListDetails[0].OwnerName;
                setCheckListName(editchecklistName);
               
                const projectId = [...loadProject];
                projectId[0]['text'] = editprojectName;
                projectId[0]['id'] = editprojectId;
                setLoadProject(projectId);
                setChecklistProject(editprojectId);
                const ownerId = [...loadOwner];
                ownerId[0]['text'] = editownerName;
                ownerId[0]['id'] = editownerId;
                setLoadOwner(ownerId);
                setChecklistOwner(editownerId);
                
               
            }
            else {
                setreason(response.data[0].Reason);
                seterrorToastFlag(true);
            }
            
        }).catch((error) => {
            console.log(error);
        })
    } catch (e) {
        console.log(e);
    }
  }


  const getTaskDetails = async (activePage) => {
    try {
        const params = {
            UserId: userData.UserId,
            RoleId: userData.RoleId,
            UserType: userData.UserType,
            AccessToken: userData.AccessToken,
            ChecklistId: paramsId.checklistId,
            Page: activePage, 
            Limit: 10
        }
        
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'edittask/', params, {
            headers: header
        }).then((response) => {
            console.log(response);
            if (response.data[0].success === 'Y') {
                settotalCount(response.data[0].TotalCount);
                const checklistTaskDetails = response.data[0].TaskDetails;
                setFormValues(response.data[0].TaskDetails);
                const loadassigneeArr = [];
                checklistTaskDetails.map( (element,index) => {
                const loadassigneeObj = {"text" : element.AssigneeName, "id": element.AssigneeId};
                loadassigneeArr.push(loadassigneeObj);
                });
                response.data[0].TotalCount > 10 ?  setshowPagination(true) :  setshowPagination(false);
                setLoadAssignee(loadassigneeArr)
               
            }
            else {
                setreason(response.data[0].Reason);
                seterrorToastFlag(true);
            }
            
        }).catch((error) => {
            console.log(error);
        })
    } catch (e) {
        console.log(e);
    }
  }

  const handlePageChange = (e) =>{
    setactivePage(e);
    getTaskDetails(e);
}

  const sendEmail = async () => {
    try {
        const params = {
            FolderId: paramsId.folderId,  
            UserId: userData.UserId,
            RoleId: userData.RoleId,
            UserType: userData.UserType,
            AccessToken: userData.AccessToken,
            CheckListId: paramsId.checklistId,
            TaskDetails: formValues,
        }
        setIsFetching(true);
    
        let header = axios.defaults.headers.post;
        axios.post(axios.defaults.baseURL + 'sendemail/', params, {
            headers: header
        }).then((response) => {
            if (response.data[0].success === 'Y') {
                setreason(response.data[0].Reason);
                settoastFlag(true);
            }
            else {
                setreason(response.data[0].Reason);
                seterrorToastFlag(true);
            }
            console.log(response);
            //setIsFetching(false);
            
        }).catch((error) => {
            console.log(error);
        })
    } catch (e) {
        console.log(e);
    }
  }
    
    let updateChecklistDetails = () => {
        try {
            const params = {
                FolderId: paramsId.folderId,  
                UserId: userData.UserId,
                RoleId: userData.RoleId,
                UserType: userData.UserType,
                AccessToken: userData.AccessToken,
                CheckListId: paramsId.checklistId,
                CheckListName:checkListName, 
                ProjectId:checklistProjectId, 
                OwnerId : ownerId,
                TaskDetails: formValues,
            }
            //console.log(params); return false;
            setIsFetching(true);
            let header = axios.defaults.headers.post;
            axios.post(axios.defaults.baseURL + 'updatetasks/', params, {
                headers: header
            }).then((response) => {
                if (response.data[0].success === 'Y') {
                    setreason(response.data[0].Reason);
                    settoastFlag(true);
                }
                else {
                    setreason(response.data[0].Reason);
                    seterrorToastFlag(true);
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            console.log(e);
        }
    }

    return ( 
        <div className='add-chk-bg'>
             {toastFlag && <Toast headline={reason} />}
            {errorToastFlag && <ErrorToast headline={reason} />}
            <div className="section">
                <div className="header_sec">
                   
                    <img src={logo} alt="Heptagon Logo" className='logo' />
                </div>
                <div className='body_sec'>
                    <div className='row'>
                        <div className='col-lg-6'>
                        <Navigation data= {navigate} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 checklist-title mb-3"><h5 className="add_checklist_here">Edit Checklist Here</h5></div>
                        <div className='col-lg-4 mt-2'>
                            <div>
                                <TextField
                                id="outlined-error"
                                name="ChecklistName"
                                label="Checklist Name"
                                value={checkListName}
                                onChange={e => setCheckListName(e.target.value)}
                                classes={{ root: classes.customTextField }}
                                />
                            
                            </div>
                        </div>
                     
                        <div className='col-lg-4 mt-2'>
                            <Autocomplete
                                multiple
                                limitTags={2}
                                id="tags-outlined"
                                options={projectList}
                                getOptionLabel={(option) => option.text}
                                filterSelectedOptions
                                defaultValue={loadProject}
                                onChange={(event, newValue) => {
                                    setChecklistProject(newValue[0]?.id);
                                    console.log(newValue);
                                  }}
                                  
                                  disableClearable={true}
                                renderInput={(params) => (
                                <TextField
                                classes={{ root: classes.customTextField }}
                                    {...params}
                                    label="Select Project"
                                    placeholder="Select Project"
                                   
                                />
                                
                                )}
                            />
                        </div>
                        <div className='col-lg-4 mt-2'>
                            <Autocomplete
                                multiple
                                key={(option) => option.id}
                                limitTags={2}
                                id="tags-outlined1"
                                options={ownerList}
                                getOptionLabel={(option) => option.text}
                                defaultValue={loadOwner}
                                filterSelectedOptions
                                onChange={(event, newValue) => {
                                    setChecklistOwner(newValue[0]?.id);
                                  }}
                                disableClearable={true}
                                renderInput={(params) => (
                                <TextField
                                classes={{ root: classes.customTextField }}
                                    {...params}
                                    label="Select Owner"
                                    placeholder="Select Owner"
                                   
                                />
                                )}
                            />
                        </div>
                    </div>
                  
                    <div className='row mt-5 mb-3'>
                        <div className="col-lg-12 checklist-title"><h5 className="add_checklist_here">Edit Task Here</h5></div>
                    </div>
                    
                    {formValues.map((element, index) => (
                    <div className="row mt-2"  key={index}>
                   
                        <div className='col-lg-6 mt-2'>
                            <div>
                                <TextField
                                key={index}
                                name="TaskName"
                                id="tskname"
                                label="Task Name"
                                value={formValues[index].TaskName}
                                onChange={(e) => handleChangeTask(index, e)}
                                 classes={{ root: classes.customTextField }}
                                />
                            </div>
                        </div>
                        
                        <div className='col-lg-4 mt-2'  key={index}>
                            <Autocomplete
                                multiple
                                name="assignee"
                                limitTags={2}
                                id="assignee"
                                options={ownerList}
                                defaultValue={[{'text': formValues[index].AssigneeName, 'id': formValues[index].AssigneeId }]}
                                getOptionLabel={(option) => option.text}
                                 onChange={(event, newValue) => {
                                    formValues.forEach((ele) => {
                                        formValues[index].AssigneeId = newValue[0]?.id;
                                    });
                                    
                                  }}
          
                                disableClearable={true}
                                filterSelectedOptions
                                renderInput={(params) => (
                                <TextField
                                classes={{ root: classes.customTextField }}
                                    {...params}
                                    label="Select Assignee"
                                    placeholder="Select Assignee"
                                    className="auto-label"
                                    
                                />
                                )}
                            />
                        </div>

                        <div className='col-lg-2 mt-3'>
                            <button className='removeformfields' onClick={() => removeFormFields(index, formValues[index].TaskId)}>Delete</button>
                            
                        </div>
                       
                      
                    </div>
                     ))}
                     <div className='row mt-3'>
                        <div className='col-lg-6'></div>
                        <div className='col-lg-4'>
                            <button className='email' onClick={() => sendEmail()}>Send Email</button>
                            <button className='add' onClick={() => addFormFields()}>Add Task</button>
                            <button className='submit' onClick={() => updateChecklistDetails()}>Update</button>
                        </div>
                        <div className='col-lg-2'></div>
                     </div>
                </div>
                { showPagination && <div className='pagination pagination-margin'>
                    <Pagination
                        activePage={activePage}
                        itemsCountPerPage={8}
                        totalItemsCount={totalCount}
                        // pageRangeDisplayed={3}
                        onChange={handlePageChange}
                        activeClass= "active"
                        activeLinkClass="activeLink"
                        />
                </div>
                }
            </div>
                                
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Delete Task</DialogTitle>
                <DialogContent>
                Are you sure you want to delete this task?
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} className='button-cancel'>No</Button>
                    <Button onClick={() => deleteTask()} className='button-primary'>Yes</Button>
                </DialogActions>
            </Dialog>
            {/* { isFetching && <Loader /> } */}
        </div>
    
     );
   
}
 
export default EditChecklist;