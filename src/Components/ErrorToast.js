import React from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const ErrorToast = (props) => {
const [openT, setOpenToast] = React.useState(true);

  
  const closeToast = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenToast(false);
  };



    return ( 

        <div>
                    <Snackbar open={openT} autoHideDuration={3000} onClose={closeToast}>
                        <Alert onClose={closeToast} severity="error" sx={{ width: '100%' }}>
                        {props.headline}
                        </Alert>
                   </Snackbar>
        </div>

     );
}
 
export default ErrorToast;